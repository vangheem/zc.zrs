ZRS Primary Storages with Blobs
===============================

When using blobs, a ZRS primary storage is a wrapper of a file storage
with blob support that also provides a network service to support
replication.  Let's create a primary storage.  We'll first create a
File Storage and wrap it in a blob storage:

    >>> import ZODB.FileStorage, ZODB.blob
    >>> fs = ZODB.FileStorage.FileStorage('Data.fs', blob_dir='blobs')

Then we'll create a primary storage using this.  A primary storage
takes the following arguments:

- The underlying storage (either a file storage or a blob storage)

- The address to listen on,

- An optional Twisted reactor to register with.  Normally, ZRS manages
  it's own reactor, but for demonstration/test purposes, we can pass
  in a special reactor that lets us demonstrate primary
  storages without actually creating network connections.

- An optional blob directory name.

Let's create a primary storage:

    >>> import zc.zrs.primary
    >>> ps = zc.zrs.primary.Primary(fs, ('', 8000), reactor=reactor)
    INFO zc.zrs.primary:
    Opening Data.fs ('', 8000)

Now, we can use this just like any other storage:

    >>> from ZODB.DB import DB
    >>> import persistent.dict
    >>> db = DB(ps)
    >>> conn = db.open()
    >>> ob = conn.root()
    >>> ob.x = persistent.dict.PersistentDict()
    >>> commit()

We can connect to it to get data that have been committed.  We're
going to connect using the test reactor:

    >>> connection = reactor.connect(('', 8000))
    INFO zc.zrs.primary:
    IPv4Address(TCP, '127.0.0.1', 47245): Connected

(We see logging output.)

The connection represents the server.  It accepts two string messages.
The first message is the protocol, which must be "zrs2.1" if blobs are
supported.  The second message is the starting transaction.  We'll
send the messages by calling the send method on the connection:

    >>> connection.send("zrs2.1")

The second message is the identifier of the last transaction seen by
the client.  We'll pass a transaction id of all zeros, indicating that
we have no data and want all of the data the storage has:

    >>> connection.send("\0"*8) # doctest: +NORMALIZE_WHITESPACE
    INFO zc.zrs.primary:
    IPv4Address(TCP, '127.0.0.1', 47245):
       start '\x00\x00\x00\x00\x00\x00\x00\x00' (1900-01-01 00:00:00.000000)

The server will send back a numer of sized messages.  Most of these
messages will be a pickles.  The testing reactor set up a transpoer
with a read method that takes care of reading each message and
unpickling it for us.  This will let us look at the server output as
data.

The first message is the transaction header for the first transaction
in the database, which is the one that creates the root object.

let's look at the first message:

    >>> message_type, data = connection.read()
    >>> message_type
    'T'
    >>> tid, status, user, description, extension = data

The transaction id is a time stamp.  We can use the the ZODB TimeStamp
module to display it.  The other data is pretty boring in this
case. :)

    >>> from ZODB.TimeStamp import TimeStamp
    >>> def ts(v):
    ...     return str(TimeStamp(v))
    >>> ts(tid), status, user, description, extension
    ('2007-03-21 20:32:57.000000', ' ', '', 'initial database creation', {})

The next message is a store message for object 0:

    >>> message_type, data = connection.read()
    >>> message_type
    'S'

    >>> oid, serial, version, data_txn = data
    >>> from ZODB.utils import u64
    >>> int(u64(oid)), ts(serial), version, data_txn
    (0, '2007-03-21 20:32:57.000000', '', None)

The data are sent as a separate (non-pickle) message:

    >>> data = connection.read(raw=True)
    >>> len(data)
    60

Finally, there will be a commit record marking the end of the
transaction:

    >>> connection.read()
    ('C', ('\x1a\x14\x0e\x11^\xf2\xe3\x15\xdb\xcb[z\x03+\xf2"',))


We can continue reading any additional transactions. In this case
there is only one:

    >>> tid, status, user, description, extension = connection.read()[1]
    >>> ts(tid), status, user, description, extension
    ('2007-03-21 20:32:58.000000', ' ', '', '', {})

    >>> for i in range(2):
    ...     oid, serial, version, data_txn = connection.read()[1]
    ...     data = connection.read(raw=True)
    ...     print (int(u64(oid)), ts(serial), version, len(data), data_txn)
    (0, '2007-03-21 20:32:58.000000', '', 78, None)
    (1, '2007-03-21 20:32:58.000000', '', 57, None)

    >>> connection.read()
    ('C', ('\x04\xc8\x8d\xd0e\xe2\xdba:\x10uTk\x85\x04\xa7',))

    >>> import time
    >>> time.sleep(0.1)
    >>> connection.have_data()
    False

If we commit more data, however, the additional data will be made
available.  We'll add some blob data.

    >>> root = conn.root()
    >>> root['blob'] = ZODB.blob.Blob()
    >>> root['blob'].open('w').write('test\n')
    >>> commit()

    >>> tid, status, user, description, extension = connection.read()[1]
    >>> ts(tid), status, user, description, extension
    ('2007-03-21 20:32:59.000000', ' ', '', '', {})

    >>> oid, serial, version, data_txn = connection.read()[1]
    >>> data = connection.read(raw=True)
    >>> print (int(u64(oid)), ts(serial), version, len(data), data_txn)
    (0, '2007-03-21 20:32:59.000000', '', 120, None)

The next message is a store blob message:

    >>> message_type, (oid, serial, version, data_txn, blocks
    ...                                ) = connection.read()

    >>> data = connection.read(raw=True)
    >>> message_type, int(u64(oid)), ts(serial), version, len(data), data_txn, blocks
    ('B', 2, '2007-03-21 20:32:59.000000', '', 21, None, 1L)

The store blob message is like a store message, but also contains the
number of blocks to follow. This blob was rather small, so there's
only one block:

    >>> connection.read(raw=True)
    'test\n'

    >>> connection.read()
    ('C', ("\x86:\x0f\x97<`\xb5l'\x87/:\xa4\xa5\xc6\x1c",))


Let's try some bigger blobs:

    >>> import random, struct
    >>> random.seed(0)
    >>> for i in range(5):
    ...     wdata = ''.join(struct.pack(">I", random.randint(0, 1<<32))
    ...                     for i in range(random.randint(1000, 100000)))
    ...     root['blob'] = ZODB.blob.Blob()
    ...     root['blob'].open('w').write(wdata)
    ...     commit()
    ...     tid, status, user, description, extension = connection.read()[1]
    ...     print (ts(tid), status, user, description, extension)
    ...     oid, serial, version, data_txn = connection.read()[1]
    ...     data = connection.read(raw=True)
    ...     print (int(u64(oid)), ts(serial), version, len(data), data_txn)
    ...     message_type, (oid, serial, version, data_txn, blocks
    ...                              ) = connection.read()
    ...     data = connection.read(raw=True)
    ...     print (message_type, int(u64(oid)), ts(serial), version, len(data),
    ...            data_txn, blocks)
    ...     rdata = ''.join(connection.read(raw=True) for i in range(blocks))
    ...     print rdata == wdata
    ...     print repr(connection.read())
    ...     print
    ('2007-03-21 20:33:00.000000', ' ', '', '', {})
    (0, '2007-03-21 20:33:00.000000', '', 120, None)
    ('B', 3, '2007-03-21 20:33:00.000000', '', 21, None, 6L)
    True
    ('C', ('k\xd1]\xd6\xb3E\xbd\xa6\x0f)`\x9f\xe4 GM',))
    <BLANKLINE>
    ('2007-03-21 20:33:01.000000', ' ', '', '', {})
    (0, '2007-03-21 20:33:01.000000', '', 120, None)
    ('B', 4, '2007-03-21 20:33:01.000000', '', 21, None, 1L)
    True
    ('C', ('W\xbb.\xcd\xab(\xfc\xf5\xf3GQ|B\xedMg',))
    <BLANKLINE>
    ('2007-03-21 20:33:02.000000', ' ', '', '', {})
    (0, '2007-03-21 20:33:02.000000', '', 120, None)
    ('B', 5, '2007-03-21 20:33:02.000000', '', 21, None, 3L)
    True
    ('C', ('2-\x91\xf0\x93T\x9f\xeaLL\xa8\xd6 \xd0\x80)',))
    <BLANKLINE>
    ('2007-03-21 20:33:03.000000', ' ', '', '', {})
    (0, '2007-03-21 20:33:03.000000', '', 120, None)
    ('B', 6, '2007-03-21 20:33:03.000000', '', 21, None, 3L)
    True
    ('C', ('\xe9Cq\xab\xd3\x93\xf6\xe3\x96\x01!U`\r\xad\x10',))
    <BLANKLINE>
    ('2007-03-21 20:33:04.000000', ' ', '', '', {})
    (0, '2007-03-21 20:33:04.000000', '', 120, None)
    ('B', 7, '2007-03-21 20:33:04.000000', '', 21, None, 2L)
    True
    ('C', ('\x18\x92\xa8q\xd7\xf1\x7f\x82Q\xba\xbcW\xe3\xb1\xcc\x85',))
    <BLANKLINE>

When a primary storage is set up with a blob storage, it requires protocol 2.1:

    >>> connection2 = reactor.connect(('', 8000))
    INFO zc.zrs.primary:
    IPv4Address(TCP, '127.0.0.1', 47246): Connected

    >>> connection2.send("zrs2.0") # doctest: +NORMALIZE_WHITESPACE
    ERROR zc.zrs.primary:
    IPv4Address(TCP, '127.0.0.1', 47246):
    Invalid protocol 'zrs2.0'. Require >= 2.1
    INFO zc.zrs.primary:
    IPv4Address(TCP, '127.0.0.1', 47246):
    Disconnected <twisted.python.failure.Failure
    <class 'twisted.internet.error.ConnectionDone'>>
    INFO zc.zrs.primary:
    IPv4Address(TCP, '127.0.0.1', 47246): Closed

.. close

    >>> connection.close() # doctest: +NORMALIZE_WHITESPACE
    INFO zc.zrs.primary:
    IPv4Address(TCP, '127.0.0.1', 47245):
       Disconnected <twisted.python.failure.Failure
       <class 'twisted.internet.error.ConnectionDone'>>

    >>> ps.close() # doctest: +NORMALIZE_WHITESPACE
    INFO zc.zrs.primary:
    Closing Data.fs ('', 8000)

